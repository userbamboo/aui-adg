AUI
===

The Atlassian User Interface Framework.

Installation
------------

*All installation commands are relative to the `./auiplugin` directory.*

1. `../aui.sh install`
2. `npm install`

One liner:

    ../aui.sh install && npm install

Running Tests
-------------

We use [Karma](http://karma-runner.github.io) for running our [Qunit](http://qunitjs.com/) tests. To run the tests:

    grunt test

You can also fire up a test server to run the tests in your favourite browser:

    grunt server-test
