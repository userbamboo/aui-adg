<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <artifactId>auiplugin-parent</artifactId>
        <groupId>com.atlassian.aui</groupId>
        <version>5.5.7-SNAPSHOT</version>
    </parent>

    <artifactId>auiplugin</artifactId>
    <packaging>atlassian-plugin</packaging>

    <name>Atlassian UI Plugin</name>
    <description>An Atlassian plugin that contains the core javascript files used in Atlassian products.</description>
    <url>https://bitbucket.org/atlassian/aui</url>

    <properties>
        <atlassian.plugin.key>com.atlassian.auiplugin</atlassian.plugin.key>
        <maven.build.timestamp.format>yyyy.MM.dd HH:mm:ss Z</maven.build.timestamp.format>
        <!-- Workaround from http://jira.codehaus.org/browse/MRESOURCES-99
             Used for ${timestamp} replacements later -->
        <timestamp>${maven.build.timestamp}</timestamp>
    </properties>

    <dependencies>
        <dependency>
            <groupId>com.atlassian.aui</groupId>
            <artifactId>auiplugin-spi</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>dom4j</groupId>
            <artifactId>dom4j</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-osgi</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-webresource</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.soy</groupId>
            <artifactId>soy-template-renderer-api</artifactId>
            <scope>provided</scope>
        </dependency>

        <!-- Test dependencies -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-all</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.core</groupId>
            <artifactId>atlassian-core</artifactId>
            <scope>test</scope>
        </dependency>
        <!-- Required by transformer tests -->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>servlet-api</artifactId>
            <scope>test</scope>
        </dependency>
        <!-- Only exists to unpack soy deps for testing -->
        <dependency>
            <groupId>com.atlassian.soy</groupId>
            <artifactId>soy-template-plugin</artifactId>
            <scope>test</scope>
        </dependency>
        <!-- Only exists to unpack jquery deps for testing -->
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>jquery</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <resources>
            <!-- Need aui-adg source files for atlassian-plugin.xml -->
            <resource>
                <directory>../../src</directory>
            </resource>

            <!-- Bundled Bower components -->
            <resource>
                <directory>../../bower_components</directory>
                <includes>
                    <include>skate/dist/skate.js</include>
                    <include>tether/tether.js</include>
                </includes>
            </resource>

            <!-- Resources from aui -->
            <resource>
                <directory>${aui.location}/src</directory>
                <excludes>
                    <!-- atlassian.js is pulled in with filtering=true in the resource below -->
                    <exclude>js/atlassian.js</exclude>
                </excludes>
            </resource>

            <!-- This defines project.version transform targets. -->
            <resource>
                <directory>${aui.location}/src</directory>
                <filtering>true</filtering>
                <includes>
                    <include>js/atlassian.js</include>
                </includes>
            </resource>

            <!-- This defines project.version transform targets. -->
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
                <includes>
                    <include>atlassian-plugin.xml</include>
                    <include>version</include>
                    <include>${aui.location}/src/i18n/aui.properties</include>
                </includes>
            </resource>
        </resources>

        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>maven-amps-plugin</artifactId>
                <configuration>
                    <productVersion>${refapp.version}</productVersion>
                    <extractDependencies>true</extractDependencies>

                    <!-- we invoke YUI explicitly below -->
                    <compressResources>false</compressResources>
                    <systemPropertyVariables>
                        <plugin.resource.directories>${basedir}/src/main/resources,${aui.location}/src</plugin.resource.directories>
                    </systemPropertyVariables>
                </configuration>
            </plugin>

            <plugin>
                <groupId>net.sf.alchim</groupId>
                <artifactId>yuicompressor-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <goals>
                            <goal>compress</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <!-- Everything on one line -->
                    <linebreakpos>-1</linebreakpos>
                    <!-- Turning off JSlint warnings -->
                    <jswarn>false</jswarn>
                    <excludes>
                        <exclude>*.xml</exclude>
                        <exclude>**/src/test/*</exclude>
                        <exclude>**/src/samples/*</exclude>
                    </excludes>
                </configuration>
            </plugin>

            <!-- unpack lib dependencies to make available to qunit -->
            <plugin>
                <artifactId>maven-dependency-plugin</artifactId>
                <executions>
                    <execution>
                        <id>extract-lib-dependencies</id>
                        <phase>generate-test-resources</phase>
                        <goals>
                            <goal>unpack-dependencies</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${project.build.directory}/qunit/dependencies</outputDirectory>
                            <includeGroupIds>com.atlassian.plugins</includeGroupIds>
                            <includeArtifactIds>jquery</includeArtifactIds>
                            <includes>**/*.js</includes>
                        </configuration>
                    </execution>
                    <execution>
                        <id>extract-soyutils-dependency-for-demos</id>
                        <!--run during generate-resources phase so that demos.json generation can use it too-->
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>unpack-dependencies</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${project.build.directory}/soyutils</outputDirectory>
                            <includeGroupIds>com.atlassian.soy</includeGroupIds>
                            <includeArtifactIds>soy-template-plugin</includeArtifactIds>
                            <includes>**/soyutils.js</includes>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>com.atlassian.lesscss</groupId>
                <artifactId>lesscss-maven-plugin</artifactId>
                <version>1.5.1</version>
                <executions>
                    <execution>
                        <id>compile-aui-less</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>compile</goal>
                        </goals>
                        <configuration>
                            <sourceDirectory>${aui.location}/src</sourceDirectory>
                        </configuration>
                    </execution>
                    <execution>
                        <id>compile-adg-less</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>compile</goal>
                        </goals>
                        <configuration>
                            <sourceDirectory>${basedir}/../../src</sourceDirectory>
                        </configuration>
                    </execution>
                </executions>
                <configuration>
                    <excludes>
                        <exclude>less/batch/*</exclude>
                        <exclude>less/imports/*</exclude>
                    </excludes>
                    <outputDirectory>target/classes</outputDirectory>
                </configuration>
            </plugin>

            <plugin>
                <artifactId>maven-source-plugin</artifactId>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals><goal>jar-no-fork</goal></goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <artifactId>maven-javadoc-plugin</artifactId>
                <executions>
                    <execution>
                        <id>attach-javadocs</id>
                        <goals><goal>jar</goal></goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>
