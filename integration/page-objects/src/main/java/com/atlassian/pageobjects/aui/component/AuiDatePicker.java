package com.atlassian.pageobjects.aui.component;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.webdriver.waiter.Waiter;

import com.google.common.base.Preconditions;
import org.openqa.selenium.By;

import javax.inject.Inject;

/**
 * @since 3.7
 *
 * @deprecated Previously published API deprecated as of AUI 5.0. Do not use outside AUI. Will be refactored out eventually.
 */
@Deprecated
public class AuiDatePicker
{
    @Inject
    PageElementFinder elementFinder;

    @Inject
    PageBinder binder;

    @Inject
    Waiter poller;

    private final By dateFieldLocator;

    private PageElement dateField;
    private PageElement calendar;

    private String uuid;

    public AuiDatePicker(By dateFieldLocator)
    {
        this.dateFieldLocator = Preconditions.checkNotNull(dateFieldLocator,
                "The by locator for the datepicker cannot be null.");
    }

    @Init
    private void init()
    {
        dateField = elementFinder.find(dateFieldLocator);
        uuid = dateField.getAttribute("data-aui-dp-uuid");
        calendar = elementFinder.find(By.cssSelector("div[data-aui-dp-popup-uuid='" + uuid +  "']"));
    }

    /**
     * It activates the date picker by selecting the date picker field.
     * @return an instance of the {@link AuiDatePicker}
     */
    public AuiDatePickerCalendar open()
    {
        if (!isOpen())
        {
            dateField.click();
        }

        return binder.bind(AuiDatePickerCalendar.class, this, uuid);
    }

    public boolean isOpen()
    {
        return calendar.isPresent() && calendar.isVisible();
    }

    /**
     * Allows direct setting of the date field instead of requiring to use the date picker
     * widget.
     * @param date the date that should be set in the date picker field.
     * @return an instance of the {@link AuiDatePicker}
     */
    public AuiDatePicker setDate(String date)
    {
        dateField.type(date);
        return this;
    }

    /**
     * @return the value in the date field.
     */
    public String getDate()
    {
        return dateField.getValue();
    }

}
