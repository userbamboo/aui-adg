define("SoySupportCtrl", ["MainModule"], function(sandboxModule){
    sandboxModule.controller("SoySupportCtrl", SoySupportCtrl);
    function SoySupportCtrl($scope) {

        $scope.initSoySupport = function() {
            $scope.soySupporter.insertSoyBoilerplate($scope.editors.soy);
        }

        $scope.initSoySupport();
    }

    var SoySupporter = {
        init: function() {
            var soySupporter = {
                compiledAuiSoys: undefined,
                enable: false,
                htmlPanelNamespace: "aui.sandbox.panel.html",
                soyPanelNamespace: "aui.sandbox.demo",
                soyErrorPlaceholder: jQuery("#soy-error-placeholder"),
                soyThrottleRate: 500,
                insertSoyBoilerplate: this._insertSoyBoilerplate,
                makeSoyCompileRequest: this._makeSoyCompileRequest,
                handleSoySuccess: this._handleSoySucces,
                handleSoyError: this._handleSoyError,
                insertSoyContents: this._insertSoyContents,
                toggleError: this._toggleError
            };

            var requestXhr = this._getCompiledAuiSoys();
            requestXhr.done(function(data) {
                var jsonData = JSON.parse(data);
                var jsData = [];
                _.each(jsonData, function(data) {
                    jsData.push(data);
                });
                soySupporter.compiledAuiSoys = (jsData.join(""));
            });
            requestXhr.fail(function(e) {
                console.warn("Fetching of compiled AUI Soys have failed");
            });

            return soySupporter;
        },

        _insertSoyBoilerplate: function(soyEditor) {
            /*
             Add soy template boiler plate
             */
            var soyContent = soyEditor.getValue();
            if (_.isEmpty(soyContent)) {
                var soyBoilerplate =
                    "/**\n" +
                        "* You should not change this namespace :)\n" +
                        "*/\n" +
                        "{namespace " + this.soyPanelNamespace +"}\n\n";
                soyEditor.insert(soyBoilerplate);
            }
        },

        _getCompiledAuiSoys: function() {
            var requestXhr = jQuery.post('/soy', { auiOnly: true });
            return requestXhr;
        },

        /**
         * data.content
         * data.shouldEncode
         * @return Promise
         */
        _makeSoyCompileRequest: function(data) {
            var deferred = jQuery.Deferred();

            if (this.enable && !(_.isEmpty(data.content) ||  data.content === "{namespace aui.sandbox.demo}\n\n")) {
                if (data.shouldEncode) {
                    var template = encodeURIComponent(data.content);
                } else {
                    var template = data.content;
                }
                var requestXhr = jQuery.post('/soy', {
                    template: template
                });
                requestXhr.done(deferred.resolve);
                requestXhr.fail(deferred.reject);
            } else {
                deferred.resolve();
            }

            return deferred;
        },

        /**
         * @param options
         * @param options.editors
         * @param options.iframe
         * @param options.iframe.iframeWindow
         * @param options.iframe.iframeDocument
         * @param options.iframe.$head
         */
        _insertSoyContents: function(options) {
            var editors = options.editors
            var soyContent = editors.soy;

            var htmlContent =
                "{namespace " + this.htmlPanelNamespace + "}\n" +
                    "/**\n" +
                    "*/\n" +
                    "{template .master}\n" +
                    editors.html + "\n" +
                    "{/template}";

            var content = {
                soy: soyContent,
                html: htmlContent
            };

            var deferred = this.makeSoyCompileRequest(_.defaults({
                content: JSON.stringify(content)
            }));

            var instance = this;
            options.iframe.iframeWindow.onerror = function(err) {
                instance.handleSoyError({
                    errorType: "js",
                    failData: {
                        responseText: err
                    }
                })
            };
            this.soyErrorPlaceholder.addClass("hidden");

            deferred.done(function(successData) {
                instance.handleSoySuccess({
                    editors: editors,
                    successData: successData,
                    iframe: options.iframe
                })
            });

            deferred.fail(function(failData) {
                instance.handleSoyError({
                    errorType: "soy",
                    failData: failData
                })
            });
        },

        /**
         * @param options.successData
         * @param options.editors
         * @param options.iframe
         * @param options.iframe.iframeDocument
         * @param options.iframe.$head
         */
        _handleSoySucces: function(options) {
            var editors = options.editors;
            var successData = options.successData;

            var dataMap = JSON.parse(successData);
            var jsInjectData = [];
            jsInjectData.push(
                this.compiledAuiSoys,
                dataMap.soy,
                dataMap.html,
                "jQuery('body').html(" + this.htmlPanelNamespace +".master());",
                editors.js
            );

            script = options.iframe.iframeDocument.createElement('script');
            script.type = 'text/javascript';
            script.text = jsInjectData.join("\n");
            options.iframe.$head[0].appendChild(script);
        },

        /**
         * @param options.failData
         * @param options.errorType
         */
        _handleSoyError: function(options) {
            var $ele = this.soyErrorPlaceholder;
            this.toggleError(_.defaults({
                value: true
            }, options));
            var dialog = AJS.InlineDialog($ele, 2,
                function(content, trigger, showPopup) {
                    content.css({"padding":"16px"}).html('<p>' + options.failData.responseText + '</p>');
                    showPopup();
                    return false;
                }, {onHover: true}
            );
            $ele.mouseout(function() {
                dialog.hide();
            });
        },

        /**
         * @param options.value {boolean}
         * @param options.errorType {"soy" | "js"}
         */
        _toggleError: function(options) {
            var $ele = this.soyErrorPlaceholder;

            if (options.value) {
                $ele.removeClass("hidden");
                if (options.errorType === "js") {
                    $ele.text("JavaScript Error");
                } else if (options.errorType === "soy") {
                    $ele.text("Soy Error");
                }
            } else {
                $ele.addClass("hidden");
            }
        }
    };

    return SoySupporter;
});