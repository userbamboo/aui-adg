define("ComponentPanelCtrl", ['MainModule'], function(sandboxModule) {

    sandboxModule.controller("ComponentPanelCtrl", ComponentPanelCtrl);

    function ComponentPanelCtrl($scope) {

        //Populate the library with all available components
        $scope.components = [];
        $scope.patterns = [];
        for(i in SANDBOX.Library.components){
            $scope.components.push({
                id: i,
                title: i.charAt(0).toUpperCase() + i.slice(1),
                display: true
            });
        }

            for(i in SANDBOX.Library.patterns){
            $scope.patterns.push({
                id: i,
                title: i.charAt(0).toUpperCase() + i.slice(1),
                display: true
            });
        }

        $scope.components.sort(alphabetically);
        $scope.patterns.sort(alphabetically);

        function alphabetically(a,b){
            if(a.title < b.title){
                return -1;
            }
            if(a.title > b.title){
                return 1;
            }
            return 0;
        }

        $scope.addComponentToEditor = function(componentId) {
            $scope.updateSnippetId(null);
            $scope.updateCurrentComponent(componentId);
            
            var data = SANDBOX.Library.find(componentId);

            var jsEditor = $scope.editors.js;
            var htmlEditor = $scope.editors.html;
            var soyEditor = $scope.editors.soy;
    
            var jsLastRow = jsEditor.getLastVisibleRow() + 1;
            var htmlLastRow = htmlEditor.getLastVisibleRow() + 1;
            
            var editors = {
                jsEditor: jsEditor,
                htmlEditor: htmlEditor,
                soyEditor: soyEditor
            }

            jsEditor.gotoLine(jsLastRow, 0, true);
            htmlEditor.gotoLine(htmlLastRow, 0, true);

            var contentData = _.defaults({
                id: componentId
            }, editors);


            $scope.cleanEditors(editors);

            if ($scope.soySupporter.enable) {
                $scope.addWithSoy(_.defaults({
                    soy: data.soy
                }, contentData));
            } else {
                $scope.addWithoutSoy(_.defaults({
                    js: data.js,
                    html: data.html
                }, contentData));
            }
        }   

        $scope.cleanEditors = function(data) {
            _.each(data, function(editor) {
                editor.setValue();
            });
        }
    
        $scope.addWithSoy = function(data) {
            if (data.soy) {
                data.soyEditor.insert(data.soy);
                /*
                 Insert boiler plate to html to call the soy
                 */
                var soyCallInHTML = "{call " + $scope.soySupporter.soyPanelNamespace + "." + data.id + "/}";
                data.htmlEditor.insert(soyCallInHTML);
            }
        }
    
        $scope.addWithoutSoy = function(data) {
            data.jsEditor.insert(data.js);
            data.htmlEditor.insert(data.html);
    
            $scope.runJavaScript();
        }
    
        $scope.runJavaScript = function() {
            //run the javascript as well
            var js = $scope.editors.js.getValue(),
                iframeWindow =  AJS.$('#output-frame')[0].contentWindow;
            iframeWindow.AJS.$('<script>').html(js).appendTo(AJS.$("body", iframeWindow.contentWindow));
        }
    }
});

