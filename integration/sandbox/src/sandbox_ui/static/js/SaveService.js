define("SaveService", ['MainModule'], function(sandboxModule){
    // Shared across all instances of saveService
    var snippets = null;

    sandboxModule.factory('saveService', function($http){

        var loadSnippetsFromLocalStorage = function() {
            var snippets = localStorage.getItem('snippets');
            return snippets ? JSON.parse(snippets) : [];
        };

        var saveServiceInstance = {

            //Publishes the code to the server and returns a url
            publishCode: function(js, css, html, soy, isSoy, callback){
                var pathSplit = window.location.pathname.split("/"),
                    id = pathSplit[1].split("-")[0],
                    url = "/code",
                    method = "POST";
                if(id){
                    url = url + "/" + id;
                    method = "PUT"
                }
                $http({
                    url: url,
                    method: method,
                    data:{
                        js: js,
                        css: css,
                        html: html,
                        soy: soy,
                        isSoy: isSoy
                    }
                }).success(function(data, status, headers, config) {
                    var pushStateString = data.id;
                    var newURL;
                    if(data.rev){
                        pushStateString = pushStateString + "-" + data.rev;
                    }
                    history.pushState({}, "saved", pushStateString);
                    newURL = window.location.href;
                    if(typeof callback === "function"){
                        callback(newURL);
                    }
                })

            },
            readURL: function(url){
                 var pathSplit = window.location.pathname.split("/"),
                     readId = pathSplit[1];
            
                return readId;
            },
            loadCodeFromServer: function(id, callback){
                    //Load the code for the given id if it exists
                    var url = "/code/" + id;

                    $http({
                        url: url,
                        method: "GET",
                    }).success(function(data, status, headers, config) {
                        if(typeof callback === "function"){
                            callback(data, status, headers, config);
                        }
                    }); 
            },
            // Saves code to localStorage and returns the snippet (incl. id and title)
            // If snippetId is null or cannot be found, saves a new snippet.
            saveCode: function(snippetId, code) {
                var snippet;

                if (snippetId != null) {
                    snippet = saveServiceInstance.getSnippetById(snippetId);
                }

                if (!snippet) {
                    var newId = saveServiceInstance.newSnippetId();
                    snippet = _.extend({}, code, {
                        id: newId,
                        title: 'Snippet ' + (newId)
                    });
                    snippets.push(snippet);
                } else {
                    _.extend(snippet, code);
                }

                localStorage.setItem('snippets', JSON.stringify(snippets));
                return snippet;
            },

            deleteSnippet: function(snippetIndex){
                var snippets = saveServiceInstance.getSnippets();
                snippets.splice(snippetIndex, 1);
                localStorage.setItem('snippets', JSON.stringify(snippets));
                return snippets;
            },

            getSnippets: function() {
                if (!snippets) snippets = loadSnippetsFromLocalStorage();

                for(i in snippets){
                    snippets[i].index = i;
                }
                return snippets;
            },

            getSnippetById: function(id) {
                return _.find(snippets, function(snippet) {
                    return snippet.id === id;
                });
            },
            newSnippetId: function(){
                var snippets = loadSnippetsFromLocalStorage(),
                    currentMax= snippets[0];
                    if(snippets.length > 0){
                        for(i in snippets){
                            if(parseInt(snippets[i].id) > parseInt(currentMax.id)) {
                                currentMax = snippets[i];
                            }
                        } 
                        
                        return currentMax.id + 1;  
                    } else {
                        return 1;
                    }
                }
            }
        return saveServiceInstance;
    });
});