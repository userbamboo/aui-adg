var http = require('http'),
    fs = require('fs'),
    request = require('request'),
    express = require('express'),
    Q = require('q');

var CONFIG = JSON.parse(fs.readFileSync(__dirname + '/config.json', 'utf8'));

var sandbox_server = express();

sandbox_server.use(express.bodyParser());

//GET
sandbox_server.get("/static/js/SaveCtrl.js", function(req, res){
	fs.readFile(__dirname + "/sandbox_ui/static/js/SaveCtrl-Live.js", function(error, content) {
            res.writeHead(200, { 'Content-Type': "application/javascript" });
            res.end(content, 'utf-8');    
        });
});

sandbox_server.get("/component-output.js", function(req, res){
    fs.readFile(__dirname + "/sandbox_ui/component-output.js", function(error, content) {
        res.writeHead(200, { 'Content-Type': "application/javascript" });
        res.end(content, 'utf-8');
    });
})

sandbox_server.get("/component-output.css", function(req, res){
    fs.readFile(__dirname + "/sandbox_ui/component-output.css", function(error, content) {
        res.writeHead(200, { 'Content-Type': "text/css" });
        res.end(content, 'utf-8');
    });
})

sandbox_server.get("/code/:hash-:rev", function(req, res){
	var hash = req.params.hash,
		revision = req.params.rev;
	getCodeRevision(hash, revision, res);
});

sandbox_server.get("/code/:hash", function(req, res){
	var hash = req.params.hash;
	getCode(hash, null, res);
});

sandbox_server.get("/:hash", function(req, res){
	fs.readFile(__dirname + "/sandbox_ui/index.html", function(error, content) {
            res.writeHead(200, { 'Content-Type': "text/html" });
            res.end(content, 'utf-8');    
        });
})

sandbox_server.get("/:hash/:rev", function(req, res){
	fs.readFile(__dirname + "/sandbox_ui/index.html", function(error, content) {
            res.writeHead(200, { 'Content-Type': "text/html" });
            res.end(content, 'utf-8');    
        });
})

//serve AUI
sandbox_server.use("/aui", express.static(__dirname+'/aui/'));
sandbox_server.use("/aui-next", express.static(__dirname+'/aui-next/'));

//Serve UI
sandbox_server.use(express.static(__dirname+'/sandbox_ui/'));


//POST
sandbox_server.post("/soy", function(req, res){
    var auiOnly = req.body.auiOnly;
    var template = req.body.template;
    if (auiOnly) {
        var promise = sendSoyCompileRequest({
            path: CONFIG.soyAUIInitPath,
            requestMethod: 'GET'
        });
    } else if (template) {
        var promise = sendSoyCompileRequest({
            path: CONFIG.soyCompileRequestPath,
            requestMethod: 'POST',
            template: template
        });
    }

    promise.done(function (data) {
        if(typeof data.data === "string"){
            res.writeHead(data.statusCode, { 'Content-Type': "text/plain" });
            res.end(data.data, 'utf-8');
        }
    });
});

sandbox_server.post("/code", function(req, res){
	var saveData = {
		js: req.body.js,
		css: req.body.css,
		html: req.body.html,
        soy: req.body.soy,
        isSoy: req.body.isSoy
	};
	generateNewId(function(newId){
		saveCode(newId, saveData, res);
	});
});

sandbox_server.put("/code/:hash", function(req, res){
	var saveData = {
		js: req.body.js,
		css: req.body.css,
		html: req.body.html,
        soy: req.body.soy ,
        isSoy: req.body.isSoy
	};

	//find the latest revision
	request({
		url: 'http://' + CONFIG.host + ':' + CONFIG.dbport + '/' + CONFIG.db + '/' + req.params.hash, 
		method: "GET"},
		function(err, response, data){
			saveData._rev = JSON.parse(data)._rev;
			saveCode(req.params.hash, saveData, res);	
		}

);

	
});

var port = process.env.PORT || CONFIG.port;
sandbox_server.listen(port, function() {
    console.log("Listening on " + port);
});

function getCodeRevision(codehash, revision, res){
	var get_revisions_url = 'http://' + CONFIG.host + ':' + CONFIG.dbport + '/' + CONFIG.db + '/' + codehash + "?revs_info=true";	
	request(get_revisions_url, function(err, response, data){
		var revInfo = JSON.parse(data)._revs_info;
		getCode(codehash, revInfo[revInfo.length-revision].rev, res);
		
	});
}

function getCode(codehash, revision, res){
	var request_url = 'http://' + CONFIG.host + ':' + CONFIG.dbport + '/' + CONFIG.db + '/' + codehash;

	if(revision){
		request_url = request_url + "?rev=" + revision;
	}
        request(
        	{
        		url: request_url,
        		method: "GET"
        	}, 
        	function(err, response, data){
                var result = JSON.parse(data),
                        code = JSON.stringify({
                                js: result.js,
                                html: result.html,
                                css: result.css,
                                soy: result.soy,
                                isSoy: result.isSoy
                        });
                res.writeHead(200, {'Content-Type': "application/json"});
                
                res.write(code);
                res.end();
        });
}

function saveCode(codehash, code, res){

	var put_url = 'http://' + CONFIG.host + ':' + CONFIG.dbport + '/' + CONFIG.db + '/' + codehash;
	request(
        {
        	url: put_url,
        	method: "PUT",
        	body: JSON.stringify(code)
        }, 
        function(err, response, data){
        	var responseObj = JSON.parse(data);
        	var obj = {id: responseObj.id};
        	var rev = responseObj.rev.split("-")[0];

        	if(parseInt(rev) > 1){
        		obj.rev = rev;
        	}
            res.writeHead(200, {'Content-Type': "application/json"});
            res.write(JSON.stringify(obj));
            res.end();
    	}
    );

}

function generateNewId(callback){
	request("http://" + CONFIG.host + ":" + CONFIG.dbport + "/_uuids", function(err, response, data){
		var newID = JSON.parse(data).uuids[0];
		checkID(newID, function(exists, newID){
			if(!exists){
				if(typeof callback==="function"){
					callback(newID);
				}
			}
		});	
	});

}

function checkID(id, callback){
	request("http://" + CONFIG.host + ":" + CONFIG.dbport + "/" + CONFIG.db + "/" + id, function(err, response, data){
		if(typeof callback==="function"){
			callback(data.error=="not_found", id);
		}
	});	
}

/**
 * @param options
 * @param options.template
 * @param options.requestMethod
 * @param options.path
 * @return promise
 */
function sendSoyCompileRequest(options) {
    var deferred = Q.defer();
    var response = "";

    var postOptions = {
        host: process.env.SOY_HOST || CONFIG.soyHost,
        port: process.env.SOY_PORT || CONFIG.soyPort,
        path: options.path,
        method: options.requestMethod
    };

    var req = http.request(postOptions);
    req.on('response', function(res) {
        res.setEncoding('utf8');

        res.on('data', function(data) {
            response += data;
        });

        res.on('end', function() {
            var map = {
                data: response,
                statusCode: res.statusCode
            };
            deferred.resolve(map);
        });
    });

    req.on('error', function(data) {
        var map = {
            data: data,
            statusCode: 400
        };
        deferred.resolve(map);
    });

    if (options.template) {
        var data = "template=" + options.template;
        req.write(data);
    }

    req.end();

    return deferred.promise;
}