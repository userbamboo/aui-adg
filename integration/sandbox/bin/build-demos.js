var fs = require("fs");
var demoDirectory = process.argv[2];
var targetDirectory = process.argv[3];
var demoFiles = fs.readdirSync(demoDirectory);

//setup directories
if(fs.existsSync(targetDirectory)){
    console.info("Found " + targetDirectory + ", removing...")
    deleteFolderRecursive(targetDirectory);
}

console.info("Creating file structure");
console.time("Created File Structure")
fs.mkdirSync(targetDirectory);
fs.mkdirSync(targetDirectory + "/html");
fs.mkdirSync(targetDirectory + "/javascript");
fs.mkdirSync(targetDirectory + "/soy");
console.timeEnd("Created File Structure");

console.time("Processed Demo Soy Files");

for (var i in demoFiles){
    eval(fs.readFileSync(demoDirectory+"/"+demoFiles[i]).toString());
}
for(var j in demo) {
    processDemo(demo, j);
}

console.timeEnd("Processed Demo Soy Files");

function processDemo(demo, demoName){
    var demo = demo[demoName];
    console.log("Processing", demoName, "demo");
    if(demo.html){
        outputDemo(demoName, "html");
        console.info("\t --> Found HTML");

    }
    if(demo.soy){
        outputDemo(demoName, "soy");
        console.info("\t --> Found Soy");
    }
    if(demo.javascript){
        outputDemo(demoName, "javascript");
        console.info("\t --> Found Javascript");
    }
}

function outputDemo(demoName, type){
    var outputTemplate = "{namespace auiSandbox." + type + "}\n" +
    "/**\n" +
     "* The demo\n" +
     "*/\n" +
    "{template ." + demoName + "}\n" +
    "{call demo." + demoName + "." + type +"/}\n" +
    "{/template}\n";
    var outputFile =  targetDirectory + "/" + type +  "/" + demoName + ".soy";
    fs.appendFileSync(outputFile, outputTemplate);
}

function deleteFolderRecursive(path) {
    if( fs.existsSync(path) ) {
        fs.readdirSync(path).forEach(function(file,index){
            var curPath = path + "/" + file;
            if(fs.statSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};


