package it.com.atlassian.aui.javascript.pages;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.elements.PageElementFinder;

import javax.inject.Inject;

/**
 * @since 3.7
 */
public abstract class TestPage implements Page
{
    @Inject
    PageElementFinder elementFinder;

    public PageElementFinder getElementFinder()
    {
        return elementFinder;
    }
}
