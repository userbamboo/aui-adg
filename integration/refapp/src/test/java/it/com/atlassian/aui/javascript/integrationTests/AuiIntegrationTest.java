package it.com.atlassian.aui.javascript.integrationTests;

import it.com.atlassian.aui.javascript.AbstractAuiIntegrationTest;
import it.com.atlassian.aui.javascript.pages.IntegrationTestPage;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @since 4.0
 */
public class AuiIntegrationTest extends AbstractAuiIntegrationTest
{
    IntegrationTestPage integrationTestPage;

    @Before
    public void setUp()
    {
        integrationTestPage = product.visit(IntegrationTestPage.class);
    }

    @Test
    public void testContextPathWorks()
    {
        assertEquals("Expects that the context path is set to /ajs", "/ajs", integrationTestPage.getContextPath());
    }
}
