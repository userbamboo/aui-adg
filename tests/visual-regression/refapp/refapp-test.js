require('../visual-regression.js')(casper).run(
    'http://localhost:9999/ajs/plugins/servlet/ajstest/test-pages',
    {
        'experimental/avatar/index.soy': ['body'],
        'experimental/aui-badge/aui-badge.html': ['body'],
        'experimental/buttons/index.soy': ['body'],
        'experimental/date-picker/date-picker-test.html': ['body'],
        'experimental/dialog-test.html': ['body'],
        'dropdown/dropdown-test.html': ['body'],
        'dropdown2/index.soy': ['body'],
        'forms/default.html': ['body'],
        'experimental/page-layout/aui-layout-groups.html': ['body'],
        'experimental/page-layout/aui-header.html': ['body'],
        'i18n/font-stacks/index.soy': ['body'],
        'icons/index.soy': ['body'],
        'inline-dialog/inline-dialog-test.html': ['body'],
        'keyboardshortcuts/keyboardshortcuts-test.html': ['body'],
        'experimental/lozenges/aui-lozenge.html': ['body'],
        'messages/messages-test.html': ['body'],
        'miscellaneous/miscellaneous-test.html': ['body'],
        'experimental/page-layout/aui-navigation.html': ['body'],
        'experimental/page-layout/aui-pageheader.html': ['body'],
        'tables/tables-test.html': ['body'],
        'tabs/tabs-test.html': ['body'],
        'toolbar/toolbar-test.html': ['body'],
        'experimental/toolbar2/toolbar2-test.html': ['body']
    }
);
