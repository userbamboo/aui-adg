AUI ADG
=======

Extension of AUI to implement the Atlassian Design Guidelines.

Requirements
------------

- Node 0.10+
- NPM 1.3+
- Bower 1.2+
- Grunt CLI 0.1+


Contributing
------------

1. Create a new branch using this format: `AUI-1234-description-of-the-change`
    - Stable branches [x.x.x] for bug fixes corresponding to the version which the bug affects
    - Feature branches [epic/*] for adding features to an epic
2. Install dependencies

        npm install

3. If you need to modify AUI source as well:

    1. run `bower link` in your local AUI repo
    2. run `bower link aui` in your local AUI-ADG repo

4. Start the server so you can see your changes

        grunt refapp

    Use `grunt refapp -v` if you need more verbose output

5. Make changes and reload pages to see them
6. Tests and Documentation live in the AUI repo, update those if modifying AUI source as well
7. Commit your changes ensuring you add the issue key to the message, eg: git commit -m "AUI-1234 updated README"
8. Submit a pull request to master or the relevant stable/epic branch

Releasing
---------

    ./build/bin/release.sh [release-version] [tag-name] [next-version]
