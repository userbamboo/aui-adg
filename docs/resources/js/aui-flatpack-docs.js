// Detect when this is embedded in a Dash.app docset as early as possible, to hide some navigational elements
if (window.dash) {
    document.documentElement.classList.add('dash-docset');
}
