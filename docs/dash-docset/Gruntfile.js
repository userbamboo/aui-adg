module.exports = function (grunt) {

  var docsetRoot     = 'AUI.docset/';
  var docsetPath     = docsetRoot + 'Contents/';
  var docsetPathDB   = docsetPath + 'Resources/docSet.dsidx';
  var docsetPathDocs = docsetPath + 'Resources/Documents/';
  var srcPath = '../docs/';

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');

  var config = {
    clean: [docsetPathDocs + '*'],
    copy: {
      docs: {
        files: [
          {
            expand: true,
            cwd: srcPath,
            src: ['resources/**', 'sandbox/**', 'docs/**'],
            dest: docsetPathDocs
          }
        ]
      },
      feed: {
        files: [
          {
            src: ['AUI.xml'],
            dest: srcPath + 'dash-docset/'
          }
        ]
      }
    }
  };

  // Register the "clean" and "copy" tasks
  grunt.initConfig(config);

  // Build docset index in a special SQLite database file
  grunt.registerTask('index', 'Build docset search index.', function () {
    var gruntDone = this.async();

    var cheerio = require('cheerio'),
      Sequelize = require('sequelize'),
      _ = require('underscore');
      fs = require('fs');

    var idxGroups;
    var nonComponentPages = ['allIn.html', 'components.html'];

    // Read the "All In" file and get out the links we want
    var apiHtml = fs.readFileSync(docsetPathDocs + 'docs/allIn.html');
    var $ = cheerio.load(apiHtml);
    var $nav = $('.aui-page-panel-nav .aui-navgroup-inner');

    idxGroups = $nav.children('.aui-nav-heading').map(function () {
      var $this = $(this);
      var group = {
        type: 'Guide',
        name: $this.text(),
        link: 'docs/allIn.html',
        children: []
      };

      // Find all methods in this group
      group.children = _.compact($this.next('.aui-nav').find('li').map(function () {
        var $link = $(this).children('a');
        var href = $link.attr('href');
        if (nonComponentPages.indexOf(href) > -1) {
          return false;
        }
        return {
          type: 'Component',
          name: $link.text(),
          link: 'docs/' + href
        };
      }));

      return group;
    });

    // Create database file
    // Most of this section copied from https://github.com/exlee/d3-dash-gen
    var db = new Sequelize('database', 'username', 'password', {
      dialect: 'sqlite',
      storage: docsetPathDB
    });

    // Create the searchIndex table
    var table = db.define('searchIndex', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: Sequelize.TEXT
      },
      type: {
        type: Sequelize.TEXT
      },
      path: {
        type: Sequelize.TEXT
      }
    }, {
      freezeTableName: true,
      timestamps: false
    });

    var errorHandler = function () {
      gruntDone(false);
    };

    table.sync({force: true}).success(function () {
      // Add the data
      var buildRowData = function (data) {
        return {
          name: data.name,
          type: data.type,
          path: data.link
        };
      };

      var rows = _.flatten(_.map(idxGroups, function (group) {
        return [group].concat(group.children);
      }));

      table.bulkCreate(_.map(rows, buildRowData))
        .success(gruntDone)
        .error(errorHandler);

    }).error(errorHandler);
  });

  // Simple build of a .tgz for the docset, which is stupidly complicated in Maven
  grunt.registerTask('tar', 'Build a .tgz archive of the docset', function () {
    var gruntDone = this.async();

    var tarSrc = docsetRoot;
    var tarDest = srcPath + 'dash-docset/AUI-${project.version}.tgz';

    var Targz = require('tar.gz');
    var compress = new Targz();
    compress.compress(tarSrc, tarDest, function (err) {
      gruntDone(!err);
    });
  });

  // Main tasks
  grunt.registerTask('generate', ['copy:docs', 'index', 'tar']);
  grunt.registerTask('default', ['generate', 'copy:feed']);

};
