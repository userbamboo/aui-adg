{namespace flatpack.docs}

/**
 * Test page
 **/
{template .formValidation}
    {call flatpack.docTemplate}
        {param id: 'form-validation' /}
        {param componentName: 'Form validation' /}
        {param sandboxURL: '../sandbox/index.html?component=dialog2' /}
        {param adgURL: 'https://developer.atlassian.com/design/latest/modal-dialog.html' /}
        {param resourcePrefix: '../' /}
        {param apiStatus: 'experimental' /}
        {param inCore: false /}
        {param webResource: 'com.atlassian.auiplugin:form-validation' /}
        {param versionAvailable: '5.5' /}
        {param summary}
            <p>Form validation is used to provide an interface for validating form fields, and displaying feedback on problems.</p>
        {/param}
        {param examples}
            {call flatpack.example}
                {param html}
                    {call demo.formValidation.messageLength /}
                {/param}
            {/call}
        {/param}
        {param code}
            <h4>HTML</h4>
            <p>Create a field that you want to be validated</p>
            {call flatpack.docExample}
                {param html}
                    {call demo.formValidation.messageLength /}
                {/param}
            {/call}
            <p>Note the use of the <code>data-aui-validate-minlength</code> attribute. This attribute specifies
            minimum length of the input value (the length must be at least 10 characters, or validation will fail).</p>

            <p>This is enough for validation to be set up on the field &ndash; no further initialisation is necessary. The
            library will take care of binding events and adding markup to any input with a <code>data-aui-validate-*</code> data attribute.</p>
        {/param}
        {param options}
            <h4>Markup configuration</h4>
            <p>Form validation arguments and options are expressed through markup.</p>
            <h5>Provided validators</h4>
            <p>Arguments for the provided validators are configured in data attributes beginning with the <code>data-aui-validate-*</code>
            prefix.</p>
            <table class="aui">
                <tr>
                    <th scope="col">Data attribute</th>
                    <th scope="col">Description</th>
                    <th scope="col">Example usage</th>
                </tr>
                <tr>
                    <td>
                        <ul>
                            <li><code>data-aui-validate-minlength</code></li>
                            <li><code>data-aui-validate-maxlength</code></li>
                        </ul>
                    </td>
                    <td>The length of the field's value must be less than or equal to the <code>minlength</code>,
                    and greater than or equal to the <code>maxlength</code>.</td>
                    <td>
                        {call flatpack.docExample}
                        {param html}{call demo.formValidation.minMaxLengthExample /}{/param}
                        {/call}
                    </td>
                </tr>
                <tr>
                    <td><code>data-aui-validate-matchingfield</code></td>
                    <td>The <code>id</code> of an input that must have a matching value</td>
                    <td>
                        {call flatpack.docExample}
                        {param html}{call demo.formValidation.matchingFieldExample /}{/param}
                        {/call}
                    </td>
                </tr>
                <tr>
                    <td><code>data-aui-validate-doesnotcontain</code></td>
                    <td>Some text that the value of the field cannot contain</td>
                    <td>
                        {call flatpack.docExample}
                        {param html}{call demo.formValidation.doesNotContainExample /}{/param}
                        {/call}
                    </td>
                </tr>
                <tr>
                    <td><code>data-aui-validate-pattern</code></td>
                    <td>A regex pattern that the field must match</td>
                    <td>
                        {call flatpack.docExample}
                        {param html}{call demo.formValidation.patternExample /}{/param}
                        {/call}
                    </td>
                </tr>
                <tr>
                    <td><code>data-aui-validate-required</code></td>
                    <td>This is a required field, and cannot have an empty value</td>
                    <td>
                        {call flatpack.docExample}
                        {param html}{call demo.formValidation.requiredFieldExample /}{/param}
                        {/call}
                    </td>
                </tr>
                <tr>
                    <td>
                        <ul>
                            <li><code>data-aui-validate-min</code></li>
                            <li><code>data-aui-validate-max</code></li>
                        </ul>
                    </td>
                    <td>The numerical value of this field must be greater than or equal to this minimum. Note that
                     it is different to <code>minlength / maxlength</code>, as it compares a number's value, rather than a string's
                     length.</td>
                    <td>
                        {call flatpack.docExample}
                        {param html}{call demo.formValidation.minMaxValueExample /}{/param}
                        {/call}
                    </td>
                </tr>
                <tr>
                    <td><code>data-aui-validate-dateformat</code></td>
                    <td>A date format that this field must comply with. Can contain any separator symbols, or the
                     following symbols, which represent different date components:
                        <ul>
                            <li>Y: Four digit year, eg. 2014</li>
                            <li>y: Two digit year, eg. 14</li>
                            <li>m: Numerical month, eg. 03</li>
                            <li>M: Abbreviated month, eg. Mar</li>
                            <li>D: Abbreviated day, eg. Mon</li>
                            <li>d: Numerical day, eg. 28</li>
                        </ul>
                    </td>
                    <td>
                        {call flatpack.docExample}
                        {param html}{call demo.formValidation.dateformatExample /}{/param}
                        {/call}
                    </td>
                </tr>
                <tr>
                    <td>
                        <ul>
                            <li><code>data-aui-validate-minchecked</code></li>
                            <li><code>data-aui-validate-maxchecked</code></li>
                        </ul>
                    </td>
                    <td>The number of checkboxes checked in this field must be greater than or equal to
                    <code>data-aui-validate-minchecked</code>, and less than or equal to
                    <code>data-aui-validate-maxchecked</code></td>
                    <td>
                        {call flatpack.docExample}
                        {param html}{call demo.formValidation.checkboxMinMaxExample /}{/param}
                        {/call}
                    </td>
                </tr>
            </table>
            <h5>Provided validators messages</h4>
            <p>All of the above validators take an additional argument: <code>data-aui-validate-...-msg</code>. This sets
            a custom message that will be shown to the user when the validation requirement is violated.</p>
            <p>Each argument is passed to <code>AJS.format</code>, with the value of the argument passed in as the first value.
            <code>{literal}{0}{/literal}</code> will be replaced with the arguments value. For example,</p>
            {call flatpack.docExample}
            {param html}
            {literal}<input type="text" data-aui-validate-pattern="http://www\..+\.com" data-aui-validate-pattern-msg="{0} is not a valid URL"</input>{/literal}
            {/param}
            {/call}
            <p>The exception to this is <code>data-aui-validate-matchingfield</code>, which is passed the first field's argument in <code>{literal}{0}{/literal}</code>
            and the second field's argument in <code>{literal}{1}{/literal}</code>.</p>
            <h5>Validation options</h5>
            <p>Options affect the behaviour of all validators running on a field. They are configured in data attributes.
            <table class="aui">
                <tr>
                    <th scope="col">Data attribute</th>
                    <th scope="col">Description</th>
                    <th scope="col">Default</th>
                    <th scope="col">Example usage</th>
                </tr>
                <tr>
                    <td><code>data-aui-validate-when</code></td>
                    <td>The event that will trigger validation. It can be any DOM event fired on the field, such as <code>keyup</code> or <code>change</code>,
                        or a custom event that you will initiate yourself.</p><p>If you want to manually trigger validation, <code>AJS.validator.validate($field)</code> will
                        run all validators that would be triggered on a field, without the <code>data-aui-validate-when</code> event occurring</td>
                    <td><code>change</code></td>
                    <td>
                        {call flatpack.docExample}
                        {param html}{call demo.formValidation.whenExample /}{/param}
                        {/call}
                    </td>
                </tr>
                <tr>
                    <td><code>data-aui-validate-watch</code></td>
                    <td>The <code>id</code> of an additional element that can also trigger validation events (the event specified by <code>data-aui-validate-when</code>)</td>
                    <td>Unspecified (only watches self)</td>
                    <td>
                        {call flatpack.docExample}
                        {param html}{call demo.formValidation.watchExample /}{/param}
                        {/call}
                    </td>
                </tr>
                <tr>
                    <td><code>data-aui-validate-displayfield</code></td>
                    <td><p>The <code>id</code> of the element to decorate when fields become valid or invalid. Icons and
                        tooltips will be displayed on this field, but validation events and logic will remain on the
                        original field.</p>
                        </td>
                    <td>Unspecified (self)</td>
                    <td>
                        {call flatpack.docExample}
                            {param html}{call demo.formValidation.displayFieldDemo /}{/param}
                        {/call}
                        {call flatpack.docExample}
                            {param html}{call demo.formValidation.checkboxDemo /}{/param}
                        {/call}
                    </td>
                </tr>
                <tr>
                    <td><code>data-aui-validate-novalidate</code></td>
                    <td>If this argument is present, validation is completely disabled on the field</td>
                    <td>Not present</td>
                    <td>
                        {call flatpack.docExample}
                        {param html}{call demo.formValidation.novalidateExample /}{/param}
                        {/call}
                    </td>
                </tr>
                <tr>
                    <td><code>data-aui-tooltip-position</code></td>
                    <td>The position that the the tooltip will be displayed on. Can be one of 'top', 'bottom' or 'side'.</td>
                    <td><code>"side"</code></td>
                    <td>
                    {call flatpack.docExample}{param html}{call demo.formValidation.tooltipPositionExample /}{/param}{/call}
                    </td>
                </tr>
            </table>
        {/param}

        {param notes}
            <h4 id="submission">Form submission</h4>
            <p>To prevent users from submitting invalid forms, the form validation library will intercept <code>submit</code>
                events that contain invalid, validating, or unvalidated elements. If the form is still being validated
                when a <code>submit</code> event occurs, submission will be delayed until validation completes.</p>
            <p>When the form is submitted in a valid state, the event <code>aui-valid-submit</code> is triggered. If the
            submit event should be prevented, preventing the <code>aui-valid-submit</code> event will prevent
            <code>submit</code> too.
            {call flatpack.docExample}
                {param html}{call demo.formValidation.validSubmitHTML /}{/param}
            {/call}
            {call flatpack.docExample}
                {param js}{call demo.formValidation.validSubmitJS /}{/param}
            {/call}

            <h4>Field events</h4>
            <p>A number of additional events are triggered on fields using the form validation library.</p>
            <table class="aui">
                <tr>
                    <th scope="col">Event name</th>
                    <th scope="col">Description</th>
                    <th scope="col">Example usage</th>
                </tr>
                <tr>
                    <td><code>aui-stop-typing</code></td>
                    <td><p>Triggered on a field when there have been no <code>keyup</code> events for some period of time.
                     Can be used as the <code>data-aui-validate-when</code> option for validators.</p>
                     </td>
                    <td>
                        {call flatpack.docExample}
                        {param html}{literal}<input data-aui-validate-when="keyup" data-aui-validate-minlength="5" class="text" type="text">{/literal}{/param}
                        {/call}
                    </td>
                </tr>
            </table>

            <h4>Plugin validators</h4>
            <p>Additional validators can be registered and your own validators defined. They can be synchronous or
             asynchronous, and may validate or invalidate based on an element in any way.</p>
             {call flatpack.docExample}
                 {param js}{call demo.formValidation.registerValidatorExample /}{/param}
             {/call}
             <p>To use this plugin, simply use the markup in an input field</p>
            {call flatpack.docExample}
                 {param html}{call demo.formValidation.useRegisteredValidatorExample /}{/param}
            {/call}
            <h5>Registering a validator</h5>
            <p>The <code>registerValidator</code> function takes the following arguments</p>
            <table class="aui">
                <tr>
                    <th scope="col">Argument name</th>
                    <th scope="col">Description</th>
                </tr>
                <tr>
                    <td><code>trigger</code></td>
                    <td><p>The trigger that will cause a validator to be run on an element. Can be either an array of
                    arguments to match, or a selector string to match.</p>
                    <p>If an array of validation arguments are provided, validation will be triggered on elements with the
                    corresponding data attributes specified. For example, <code>['startswith', 'endswith']</code> will match
                    <code>data-aui-validate-startswith</code> or <code>data-aui-validate-endswith</code>.</p>
                    <p>If a string is provided, validation will be triggered on elements that match this selector. For
                    example, a trigger of <code>'[aria-required]'</code> will cause the validator to also be run when the
                    <code>aria-required</code> attribute is present.</td>
                </tr>
                <tr>
                    <td><code>validationFunction</code></td>
                    <td>A function containing the logic of the validator. This function takes the argument <code>field</code>
                    (see below for more information on this).
                    </td>
                </tr>
            </table>
            <h5>Writing the validator</h5>
            <p>The function <code>validationFunction(field)</code> is the core of a validator, containing
            the logic of whether or not a field is valid. It takes the following arguments</p>
            <table class="aui">
                <tr>
                    <th scope="col">Argument name</th>
                    <th scope="col">Description</th>
                </tr>
                <tr>
                    <td><code>field</code></td>
                    <td>The field that the validator is being asked to validate. It contains:
                        <ul>
                            <li><code>field.$el</code>: a jQuery element of the field that validation has been requested on.</li>
                            <li><code>field.args</code>: An accessor function to retrieve validation arguments from the element.
                            <code>arguments('startswith')</code> will access the value for <code>data-aui-validate-startswith</code></li>
                            <li><code>field.validate()</code>: declare that this field has passed validation</li>
                            <li><code>field.invalidate(message)</code>: declare that this field has failed validation, and
                             possibly show the user the message provided.</li>
                        </ul>
                    </td>
                </tr>
            </table>
            <p>After performing whatever logic is necessary with the <code>field.$el</code> object to validate or
            invalidate, all code paths must execute either <code>field.validate()</code> or <code>field.invalidate('message')</code>.
            The reason given may be shown on the field as an error (see <code>AJS.format()</code> for formatting these strings).

        {/param}

    {/call}
{/template}