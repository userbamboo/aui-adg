module.exports = function (grunt) {
    'use strict';

    var config = require('./build/configs')(grunt);

    config.jquery = grunt.option('jquery') || '1.8.3';
    config.pkg = grunt.file.readJSON('package.json');

    grunt.initConfig(config);

    grunt.loadTasks('build/tasks');
    grunt.loadNpmTasks('grunt-available-tasks');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-phantomcss');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-text-replace');
    grunt.loadNpmTasks('grunt-debug-task');

    grunt.registerTask('default', 'Shows the available tasks in a much more user friendly manner.', 'availabletasks');
    grunt.registerTask('build', 'Builds AUI ADG.', [
        'clean:dist',
        'shell:buildAui',
        'less:dist',
        'concat:auiAll',
        'cssmin:auiDistNext',
        'cssmin:auiDistOld',
        'copy:auiDistNextJs',
        'copy:auiDistOldJs',
        'copy:auiDistNextJs',
        'copy:auiDistAssets',
        'shell:auiLegacyImages',
        'shell:auiNextLegacyImages',
        'replace:projectVersion',
        'clean:postDistClean',
        'clean:tmp'
    ]);
    grunt.registerTask('docs', 'Builds the documentation and starts a server.', [
        'install',
        'build',
        'soy-render:docs',
        'copy:docs',
        'connect:docs'
    ]);
    grunt.registerTask('visreg', 'Runs the visual regression tests against the baseline images.', [
        'clean:tmp',
        'shell:phantomCssGitClone',
        'connect:phantomcss',
        'phantomcss:docs',
        'refapp:phantomcss',
        'phantomcss:refapp',
        'shell:phantomCssGitAdd',
        'shell:phantomCssGitCommit',
        'shell:phantomCssGitPush',
        'clean:tmp'
    ]);

    grunt.verbose.writeln('using aui src from' + config.paths.aui);
};
