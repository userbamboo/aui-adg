module.exports = {
    docs: {
        options: {
            results: '.tmp/aui-visual-regression/docs/results',
            screenshots: '.tmp/aui-visual-regression/docs/screenshots'
        },
        src: ['tests/visual-regression/docs/*-test.js']
    },
    refapp: {
        options: {
            results: '.tmp/aui-visual-regression/refapp/results',
            screenshots: '.tmp/aui-visual-regression/refapp/screenshots'
        },
        src: ['tests/visual-regression/refapp/*-test.js']
    }
};
