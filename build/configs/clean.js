module.exports = {
    flatpack: [
        'dist/*.zip'
    ],
    dist: [
        'dist'
    ],
    postDistClean: [
        'dist/*',
        '!dist/aui',
        '!dist/aui-next'
    ],
    tmp: [
        '.tmp'
    ],
    target: [
        '{**/,*/}target'
    ],
    deps: [
        'node_modules',
        'bower_components'
    ]
};
