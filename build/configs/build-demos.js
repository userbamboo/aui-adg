module.exports = {
    sandbox: {
        options: {
            src: 'integration/plugin/target/demojs',
            dest: 'integration/plugin/target/classes/demos.json',
            soy: 'integration/plugin/target/soyutils/js/soyutils.js'
        }
    }
};
