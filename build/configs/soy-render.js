module.exports = {
    docs: {
        options: {
            baseDir: 'docs/soy',
            i18nBundle: '<%= paths.i18nBundle %>',
            glob: '**.soy',
            outDir: 'dist-docs',
            rootNamespace: 'flatpack',
            data: {
                auiVersion: '<%= pkg.version %>'
            },
            dependencies: [{
                baseDir: '<%= paths.aui %>/src/soy',
                glob: '**.soy'
            }, {
                baseDir: 'docs/demo',
                glob: '**.soy'
            }, {
                baseDir: 'docs/soyDependencies',
                glob: '**.soy'
            }]
        }
    }
};