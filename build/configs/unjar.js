module.exports = {
    soyutils: {
        options: {
            map: {
                'https://maven.atlassian.com/content/repositories/atlassian-public/com/atlassian/soy/soy-template-plugin/2.1.4/soy-template-plugin-2.1.4.jar': {
                    'js/soyutils.js': '.tmp/js/soyutils.js'
                }
            }
        }
    }
};
