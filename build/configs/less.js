module.exports = function (grunt) {
    var path = require('path');
    var paths = require('./paths')(grunt);

    return {
        dist: {
            options: {
                paths: ['src/less'],
                ieCompat: true,
                modifyVars: {
                    // a relative path is required for less to import the file properly
                    'aui-location': '"' + path.relative(process.cwd(), paths.aui) + '"'
                }
            },
            files: {
                'dist/aui-next/css/aui.css': '<%= paths.styleSource %>/batch/main.less',
                'dist/aui-next/css/aui-experimental.css': '<%= paths.styleSource %>/batch/experimental.less',
                'dist/aui-next/css/aui-ie.css': '<%= paths.styleSource %>/batch/ie.less',
                'dist/aui-next/css/aui-ie9.css': '<%= paths.styleSource %>/batch/ie9.less',

                // DEPRECATED: legacy flatpack.
                '.tmp/aui/css/aui.css': [
                    '<%= paths.auiStyleSource %>aui-reset.less',
                    '<%= paths.auiStyleSource %>aui-page-typography.less',
                    '<%= paths.auiStyleSource %>html5.less',
                    '<%= paths.auiStyleSource %>aui-avatars.less',
                    '<%= paths.auiStyleSource %>aui-badge.less',
                    '<%= paths.auiStyleSource %>aui-buttons.less',
                    '<%= paths.auiStyleSource %>aui-date-picker.less',
                    '<%= paths.auiStyleSource %>aui-header.less',
                    '<%= paths.auiStyleSource %>aui-lozenge.less',
                    '<%= paths.auiStyleSource %>aui-navigation.less',
                    '<%= paths.auiStyleSource %>aui-page-layout.less',
                    '<%= paths.auiStyleSource %>aui-page-header.less',
                    '<%= paths.auiStyleSource %>aui-toolbar2.less',
                    '<%= paths.auiStyleSource %>basic.less',
                    '<%= paths.auiStyleSource %>dialog.less',
                    '<%= paths.auiStyleSource %>layer.less',
                    '<%= paths.auiStyleSource %>dialog2.less',
                    '<%= paths.auiStyleSource %>dropdown.less',
                    '<%= paths.auiStyleSource %>dropdown2.less',
                    '<%= paths.auiStyleSource %>forms.less',
                    '<%= paths.auiStyleSource %>icons.less',
                    '<%= paths.auiStyleSource %>inline-dialog.less',
                    '<%= paths.auiStyleSource %>messages.less',
                    '<%= paths.auiStyleSource %>tables.less',
                    '<%= paths.auiStyleSource %>tabs.less',
                    '<%= paths.auiStyleSource %>toolbar.less',

                    // extends base AUI with ADG
                    '<%= paths.styleSource %>adg-header.less',
                    '<%= paths.styleSource %>adg-page-layout.less',
                    '<%= paths.styleSource %>adg-icons.less',
                    '<%= paths.styleSource %>adg-messages.less'
                ],
                '.tmp/aui/css/aui-experimental.css': [
                    '<%= paths.auiStyleSource %>aui-module.less',
                    '<%= paths.auiStyleSource %>aui-experimental-labels.less',
                    '<%= paths.auiStyleSource %>aui-experimental-tables-sortable.less',
                    '<%= paths.styleSource %>adg-iconfont.less',    // extends base AUI with ADG
                    '<%= paths.auiStyleSource %>aui-experimental-progress-tracker.less',
                    '<%= paths.auiCssVendorSource %>jquery/jquery.tipsy.css',
                    '<%= paths.auiStyleSource %>aui-experimental-tooltip.less',
                    '<%= paths.auiStyleSource %>aui-experimental-expander.less',
                    '<%= paths.auiStyleSource %>aui-experimental-progress-indicator.less',
                    '<%= paths.auiCssVendorSource %>jquery/plugins/jquery.select2.css',
                    '<%= paths.auiStyleSource %>aui-select2.less'
                ],
                '.tmp/aui/css/aui-ie.css': [
                    '<%= paths.auiStyleSource %>dialog-ie.less',
                    '<%= paths.auiStyleSource %>dropdown-ie.less',
                    '<%= paths.auiStyleSource %>forms-ie.less',
                    '<%= paths.auiStyleSource %>inline-dialog-ie.less',
                    '<%= paths.auiStyleSource %>toolbar-ie.less',
                    '<%= paths.auiStyleSource %>aui-toolbar2-ie.less',
                    '<%= paths.auiStyleSource %>aui-experimental-progress-tracker-ie.less'
                ],
                '.tmp/aui/css/aui-ie9.css': [
                    '<%= paths.auiStyleSource %>aui-badge-ie.less',
                    '<%= paths.auiStyleSource %>aui-header-ie.less',
                    '<%= paths.auiStyleSource %>inline-dialog-ie.less',

                    // extends base AUI with ADG
                    '<%= paths.styleSource %>adg-header-ie.less'

                ]
                // END DEPRECATED
            }
        }
    };
};
