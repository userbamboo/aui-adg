module.exports = function (grunt) {
    var path = require('path');
    var paths = require('./paths')(grunt);

    return {
        docs: {
            options: {
                base: '<%= paths.docsDist %>',
                keepalive: true,
                open: true
            }
        },
        phantomcss: {
            options: {
                base: 'integration/docs/target/docs'
            }
        }
    };
};
