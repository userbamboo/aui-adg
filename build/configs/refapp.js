module.exports = {
    debug: {
        options: {
            debug: true,
            keepalive: true
        }
    },
    normal: {
        options: {
            keepalive: true
        }
    },
    phantomcss: {
        options: {
            keepalive: false
        }
    }
};
