module.exports = {
    flatpack: {
        options: {
            archive: 'dist/aui-flat-pack-<%= pkg.version %>.zip',
            mode: 'zip'
        },
        files: [{
            expand: true,
            cwd: 'dist/',
            src: ['**/*'],
            dest: '/'
        }]
    }
};
