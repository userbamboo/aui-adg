module.exports = {
    options: {
        stripBanners: true,
        banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %> */\n\n'
    },

    // DEPRECATED Generates the old aui flatpack, remove after 6.0.
    auiAll: {
        files: {
            '.tmp/aui/css/aui-all.css': [
                '.tmp/aui/css/aui.css',
                '.tmp/aui/css/aui-experimental.css'
            ]
        }
    }
};