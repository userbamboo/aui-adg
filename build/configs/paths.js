var fs = require('fs');
module.exports = function(grunt) {
    var path = require('path');
    var auiLocation = path.resolve('bower_components/aui/');

    return {
        aui: auiLocation,
        auiCssVendorSource: auiLocation + '/src/css-vendor/',
        auiDist: auiLocation + '/dist/',
        auiStyleSource: auiLocation + '/src/less/',
        compiledSoySource: 'integration/plugin/target/qunit/soy/',
        dist: 'dist/',
        docsDist: 'dist-docs',
        i18nBundle: auiLocation + '/src/i18n/aui.properties',
        jsSource: 'src/js/',
        jsVendorSource: 'src/js-vendor/',
        styleSource: 'src/less/'
    };
};
