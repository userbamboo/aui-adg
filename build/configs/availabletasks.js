module.exports = {
    options: {
        filter: 'include',
        tasks: [
            'build',
            'build-demos',
            'clean',
            'docs',
            'install',
            'integration-tests',
            'refapp'
        ]
    },
    all: {}
};
