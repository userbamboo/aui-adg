module.exports = {
    auiDistAssets: {
        files: [{
            cwd: '<%= paths.auiDist %>' + 'aui-next/css',
            dest: '<%= paths.dist %>' + 'aui-next/css/',
            expand: true,
            filter: 'isFile',
            src: ['**/*', '!**/*.css']
        }, {
            cwd: '<%= paths.auiDist %>' + 'aui/css',
            dest:  '<%= paths.dist %>' + 'aui/css/',
            expand: true,
            filter: 'isFile',
            src: ['**/*', '!**/*.css']
        }, {
            cwd: '<%= paths.styleSource %>',
            dest:  '<%= paths.dist %>' + 'aui/css/',
            expand: true,
            filter: 'isFile',
            src: ['**/*', '!**/*.less']
        }, {
            cwd: '<%= paths.styleSource %>',
            dest:  '<%= paths.dist %>' + 'aui-next/css/',
            expand: true,
            filter: 'isFile',
            src: ['**/*', '!**/*.less']
        }]
    },
    auiDistNextJs: {
        files: [{
            cwd:  '<%= paths.auiDist %>' + 'aui-next/js',
            expand: true,
            dest:  '<%= paths.dist %>' + 'aui-next/js',
            filter: 'isFile',
            src: ['*.js']
        }]
    },
    auiDistOldJs: {
        files: [{
            cwd:  '<%= paths.auiDist %>' + 'aui/js',
            expand: true,
            dest:  '<%= paths.dist %>' + 'aui/js',
            filter: 'isFile',
            src: ['*.js']
        }]
    },
    docs: {
        files: [{
            // aui
            cwd: 'dist/aui',
            expand: true,
            dest: 'dist-docs/sandbox/aui',
            src: ['**/*']
        }, {
            // docs resources
            cwd: 'docs/resources',
            expand: true,
            dest: 'dist-docs/resources',
            src: ['**/*']
        }, {
            // sandbox resources
            cwd: 'integration/sandbox/target/sandbox/sandbox_ui',
            expand: true,
            dest: 'dist-docs/sandbox',
            src: ['**/*']
        }]
    }
};
