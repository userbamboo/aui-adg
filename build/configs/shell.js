module.exports = {
    options: {
        stdout: true,
        stderr: true
    },
    phantomCssDir: '.tmp/aui-visual-regression',
    phantomCssGitClone: {
        command: 'git clone git@bitbucket.org:atlassian/aui-visual-regression <%= shell.phantomCssDir %>'
    },
    phantomCssGitAdd: {
        command: 'cd <%= shell.phantomCssDir %> && git add .'
    },
    phantomCssGitCommit: {
        command: 'cd <%= shell.phantomCssDir %> && git commit -am "Update visual regression baseline."'
    },
    phantomCssGitPush: {
        command: 'cd <%= shell.phantomCssDir %> && git push'
    },
    auiLegacyImages: {
        command: 'cd dist/aui/css && cp -rf */* ./'
    },
    auiNextLegacyImages: {
        command: 'cd dist/aui-next/css && cp -rf */* ./'
    },
    buildAui: {
        command: 'cd <%= paths.aui %> && npm install && ./node_modules/.bin/grunt build-js && ./node_modules/.bin/grunt copy:auiFlatpackAssets'
    }
};
