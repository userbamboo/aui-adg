#!/bin/bash
set -e
[ -z "$1" ] && [ -z "$2" ] && echo "Usage: release.sh [release-version] [next-version]" && exit 1

RELEASE_VERSION=$1
NEXT_VERSION=$2

#Deploy dist to Maven.
./node_modules/.bin/grunt compress:flatpack
cd integration
mvn deploy:deploy-file -DgroupId=com.atlassian.aui -DartifactId=aui-flat-pack -Dversion=$RELEASE_VERSION -Dpackaging=zip -Dfile=../dist/aui-flat-pack-$RELEASE_VERSION.zip -DrepositoryId=atlassian-public -Durl=https://maven.atlassian.com/public
cd ..
./node_modules/.bin/grunt clean:flatpack

# Deploy plugin to Maven.
cd integration
mvn clean deploy -DskipTests
cd ..

# Update dist repo.
git clone git@bitbucket.org:atlassian/aui-adg-dist .dist
rm -rf .dist/*
rm -rf .dist/.gitignore
cp -rf dist/* .dist/
cd .dist
git add .
git commit -am "Release $RELEASE_VERSION."
git push origin master
git tag -a $RELEASE_VERSION -m "$RELEASE_VERSION"
git push --tags
cd ..
rm -rf .dist

# Bumps the version in all modules and pushes.
cd integration
mvn versions:set -DnewVersion=$NEXT_VERSION -DgenerateBackupPoms=false
git commit -am "Bump maven version to $NEXT_VERSION."
cd ..
npm version $NEXT_VERSION
git tag -d v$NEXT_VERSION
git push origin

# Push to CDN
./build/bin/push-to-cdn.sh $RELEASE_VERSION //aui-origin.herokuapp.com