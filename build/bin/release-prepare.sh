#!/bin/bash
set -e
[ -z "$1" ] && echo "Usage: release.sh [release-version]" && exit 1

RELEASE_VERSION=$1

# Set Maven version.
cd integration
mvn versions:set -DnewVersion=$RELEASE_VERSION -DgenerateBackupPoms=false
git commit -am "Prepare $RELEASE_VERSION."
git push origin
cd ..

# Set NPM version.
# We must delete and retag because NPM is too opinionated about the tag name.
npm version $RELEASE_VERSION
git tag -d v$RELEASE_VERSION
git tag -a $RELEASE_VERSION -m "Release $RELEASE_VERSION."
git push --tags
git push origin

grunt build