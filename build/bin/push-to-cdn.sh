#!/bin/bash

# check vars
[ -z "$1" ] && [ -z "$2" ] && echo "Usage: upload-to-cdn.sh.sh [release-version] [s3-bucket]" && exit 1

# variables
RELEASE_VERSION=$1
S3_BUCKET=$2

# Clone Dist
git clone git@bitbucket.org:atlassian/aui-adg-dist.git .uploadToCdn
cd .uploadToCdn
git checkout tags/$RELEASE_VERSION

gzip -9 -r -k .

# Requires some setup:
aws --profile auicdn s3 cp aui-next s3://$S3_BUCKET/aui-adg/$RELEASE_VERSION --recursive --exclude "*.gz" --cache-control "public, max-age=86400"
aws --profile auicdn s3 cp aui-next s3://$S3_BUCKET/aui-adg/$RELEASE_VERSION --recursive --exclude "*" --include "*.gz" --cache-control "public, max-age=86400"  --content-encoding="gzip"
# clean up
cd ..
rm -rf .uploadToCdn
