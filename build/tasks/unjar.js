module.exports = function(grunt) {
    'use strict';

    grunt.registerMultiTask('unjar', 'Retrieves JS dependencies that are packed away in .jar files.', function() {
        var opts = this.options({
            map: [],
            tmp: '.tmp'
        });

        var current = 0;
        var deps = opts.map;
        var done = this.async();
        var exec = require('child_process').exec;
        var fs = require('fs');
        var ival = setInterval(wait);
        var path = require('path');
        var tmp = opts.tmp;
        var total = count(deps);

        fs.mkdir(tmp, function() {
            for (var a in deps) {
                wget(a, deps[a]);
            }
        });

        function copy(base, map) {
            for (var a in map) {
                var src = base + '/' + a;

                exec('mkdir -p ' + path.dirname(map[a]) + ' && cp -rf ' + src + ' ' + map[a], function() {
                    ++current;
                });
            }
        }

        function count(deps) {
            var total = 0;

            for (var a in deps) {
                for (var b in deps[a]) {
                    ++total;
                }
            }

            return total;
        }

        function jarxf(dest, file, done) {
            exec(
                'mkdir ' + dest +
                ' && cd ' + dest +
                ' && jar xf ../../' + file +
                ' && cd ../../',
                done
            );
        }

        function wait() {
            if (current === total) {
                clearInterval(ival);
                done();
            }
        }

        function wget(url, map) {
            var file = tmp + '/' + path.basename(url),
                dest = file + '.extract';

            exec('wget -O ' + file + ' ' + url, function() {
                jarxf(dest, file, function() {
                    copy(dest, map);
                });
            });
        }
    });
};
