
module.exports = function(grunt) {
    grunt.registerMultiTask('build-demos', 'Builds the demos.json metadata and soyutils.js for the Sandbox.', function() {
        var opts = this.options();
        var fs = require('fs');
        var demoDirectory = opts.src;
        var targetFile = opts.dest;
        var soyUtilsFile = opts.soy;
        var outputObject = [];
        var soy = createSoyVarFromFile(soyUtilsFile);
        var demo = createDemoVarFromDir(demoDirectory);

        for (var j in demo) {
            var thisDemo = demo[j];

            outputObject.push({
                name: j,
                html: thisDemo.html() || '',
                js: thisDemo.javascript() || '',
                soy: thisDemo.soy ? thisDemo.soy() : ''
            });
        }

        // Write output to specified file.
        fs.writeFileSync(targetFile, JSON.stringify(outputObject));


        // creates "demo" global
        function createDemoVarFromDir(dir) {
            var code = '';
            var files = fs.readdirSync(dir);

            for (var a in files) {
                code += ';' + fs.readFileSync(demoDirectory + '/' + files[a]).toString();
            }

            return wrapeval(code, 'demo');
        }

        // creates "soy" global
        function createSoyVarFromFile(file) {
            return wrapeval(fs.readFileSync(file).toString(), 'soy');
        }

        // wraps in a closure to return the local eval'd var
        function wrapeval(code, name) {
            return eval('(function() {' + code + '; return ' + name + '; })()');
        }
    });
};
