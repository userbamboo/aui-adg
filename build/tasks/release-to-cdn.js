module.exports = function(grunt) {
    'use strict';

    var request = require('request');
    var xmlParser = require('xml2js');
    var Q = require('q');
    var _ = require('underscore');
    var fs = require('fs');
    var handlebars = require('handlebars');
    var mkdirp = require('mkdirp');
    var exec = require('child_process').exec;

    //Note we should move the push-to-cdn.sh to this task eventually, right now this task is
    //only responsible for generating the landing page for the aui-cdn

    grunt.registerMultiTask('release-to-cdn', 'Releases the AUI CDN listing file', function() {

        var done = this.async();
        getS3Objects()
            .then(outputFile)
            .then(pushToCdn)
            .then(done);

        function getS3Objects() {
            var deferred = Q.defer();
            requestS3Objects().then(function processObjects(s3Results){
                 deferred.resolve(processS3Objects(s3Results));
            });

            function processS3Objects(objectData) {
                var processedObjects = _.chain(objectData)
                    .map(extractObject)
                    .filter(nonGz)
                    .groupBy('version')
                    .map(processedObject)
                    .toArray()
                    .reverse()
                    .value();

                function extractObject(object) {
                    var result = {};
                    result.url = object.Key[0];

                    var split = result.url.split('.')
                    result.version = result.url.split('/')[1];
                    result.fileType = split[split.length - 1];
                    return result;
                }

                function nonGz(s3Object) {
                    return s3Object.fileType !== 'gz';
                }

                function processedObject(item, index, list) {
                    var result = {};
                    result.version = index;
                    result.files = item;
                    return result;
                }

                return processedObjects;
            }

            //This function is recursive
            function requestS3Objects(marker) {
                var deferred = Q.defer();
                var URL = 'http://aui-origin.herokuapp.com.s3.amazonaws.com/';
                //append pagination marker to the request if we have one
                URL = marker ? URL + '?marker=' + marker: URL;

                makeRequest(URL)
                    .then(parseXML)
                    .then(resolvePagination);

                function resolvePagination(s3Results){
                    var results = s3Results.ListBucketResult;
                    var s3Objects = results.Contents;
                    if (results.IsTruncated[0] === 'true') {
                        var marker = s3Objects[s3Objects.length - 1].Key[0];
                        requestS3Objects(marker).then(function combineResults(nextS3Objects) {
                            s3Objects = s3Objects.concat(nextS3Objects);
                            deferred.resolve(s3Objects);
                        });
                    } else {
                        deferred.resolve(s3Objects);
                    }
                }

                return deferred.promise;
            }
            return deferred.promise;
        }


        function outputFile(data) {
            var deferred = Q.defer();
            fs.readFile('build/templates/cdn-landing-page.handlebars', 'utf8', function(err, file) {
                var template = handlebars.compile(file);
                var renderedTemplate = template({auiVersions: data});
                mkdirp('.tmp/cdn', function createIndexFile(err) {
                    fs.writeFile('.tmp/cdn/index.html', renderedTemplate, function(err) {
                        grunt.log.writeln('cdn index page was saved to .tmp');
                        deferred.resolve();
                    });
                });
            });

            return deferred.promise;
        }


        function pushToCdn() {
            var deferred = Q.defer();
            var deleteCommand = 'aws --profile auicdn s3 rm s3://aui-origin.herokuapp.com/aui-adg/index.html';
            var pushCommand = 'aws --profile auicdn s3 cp .tmp/cdn/index.html s3://aui-origin.herokuapp.com/aui-adg/index.html';

            if (grunt.option('dryrun')) {
                deleteCommand = deleteCommand + ' --dryrun';
                pushCommand = pushCommand + ' --dryrun';
            }

            exec(deleteCommand, function(error, stdout, stderr) {
                grunt.log.writeln(stdout);
                exec(pushCommand, function(error, stdout, stderr) {
                   grunt.log.writeln(stdout);
                   deferred.resolve();
                });
            });

            return deferred.promise;
        }

        function makeRequest(url) {
            var deferred = Q.defer();
            request(url, function(err, resp, body) {
                deferred.resolve(body);
            })
            return deferred.promise;
        }

        function parseXML(xmlString) {
            var deferred = Q.defer();
            xmlParser.parseString(xmlString, function(err, result) {
                deferred.resolve(result);
            })
            return deferred.promise;
        }
    });

};
