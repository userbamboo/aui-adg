module.exports = function(grunt) {
    'use strict';

    var INTEGRATION_PATH = 'integration/refapp';

    var proc = require('child_process');
    var mavenStdoutFilter = require('./lib/maven-stdout-filter')(grunt);

    grunt.registerMultiTask('refapp', 'Runs the refapp in a standalone server.', function() {
        var opts = this.options({
                debug: false,
                keepalive: false
            });
        var done = this.async();
        var debug = opts.debug ? 'debug' : 'run';
        var args = [
                'amps:' + debug,
                '-DskipTests',
                '-DskipAllPrompts=true',
                '-Djquery.version=' + grunt.config('jquery')
            ];
        var auiPath = grunt.config('paths').aui;

        if (auiPath) {
            args.push('-Daui.location=' + auiPath);
        }

        grunt.verbose.writeln('mvn ' + args.join(' '));

        var cmd = proc.spawn('mvn', args, {
            cwd: INTEGRATION_PATH
        });

        cmd.stdout.on('data', mavenStdoutFilter);
        cmd.stdout.on('data', function(data) {
            var data = data.toString();

            if (data.indexOf('refapp started successfully') !== -1) {
                grunt.log.writeln();
                grunt.log.ok(data);
            } else if (data.indexOf('JDWP exit error') !== -1) {
                grunt.log.writeln();
                grunt.fail.fatal(data);
            }

            if (!opts.keepalive) {
                done();
            }
        });

        cmd.on('close', function(code) {
            done(code === 0);
        });
    });
};
