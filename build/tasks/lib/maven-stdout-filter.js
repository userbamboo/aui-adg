module.exports = function (grunt) {
    var isLastLogError = false;

    function failIfLastLogIsErrorOtherwise(fn, data) {
        if (isLastLogError) {
            fn = grunt.fail.fatal;
        }

        fn(data);
    }

    return function(data) {
        data = data.toString();

        if (data.indexOf('[INFO]') === 0) {
            failIfLastLogIsErrorOtherwise(grunt.verbose.writeln, data);
        } else if (data.indexOf('[WARNING]') === 0) {
            failIfLastLogIsErrorOtherwise(grunt.log.error, data);
        } else if (data.indexOf('[ERROR]') === 0) {
            isLastLogError = true;
            grunt.log.error(data);
        }
    };
};
