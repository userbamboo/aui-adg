module.exports = function(grunt) {
    'use strict';

    var INTEGRATION_PATH = 'integration/refapp';

    var proc = require('child_process');

    grunt.registerMultiTask('integration-tests', 'Runs the integration tests against the refapp.', function() {
        var done = this.async();
        var cmd = proc.spawn('mvn', [
            'verify',
            '-DskipAllPrompts=true',
            '-Djquery.version=' + grunt.config('jquery')
        ], {
            cwd: INTEGRATION_PATH
        });

        cmd.stdout.on('data', function(data) {
            grunt.log.writeln(data);
        });

        cmd.on('close', function(code) {
            done(code === 0);
        });
    });
};
